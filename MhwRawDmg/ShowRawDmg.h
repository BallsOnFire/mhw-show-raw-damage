#pragma once

#include "Process.h"
#include "writeln.h"

struct MhwShowRawDmg
{
	using process_t = Windows::Process;
	using module_t = Windows::Process::Module;

	process_t m_game{};

	std::array<uint8_t, 6> m_originalBytesAtk{ 0xF3,0x42,0x0F,0x59,0x04,0x81 };
	std::array<uint8_t, 6> m_patchBytesAtk{ 0x90,0x90,0x90,0x90,0x90,0x90 };

	std::array<uint8_t, 3> m_originalBytesElement{ 0x8d,0x04,0x89 };
	std::array<uint8_t, 3> m_patchBytesElement{ 0x8b,0xc1,0xc3 };

	std::array<uint8_t, 3> m_patchBytesAntiTemper{ 0xb0,0x01,0x90 };
	std::array<uint8_t, 3> m_originalBytesAntiTemper{ 0xf,0x94,0xc0 };

	std::array<uint8_t, 5> m_patchBytesFreeElement{ 0x90,0x90,0x90,0x90,0x90 };
	std::array<uint8_t, 5> m_originalBytesFreeElement{ 0x8D,0x04,0x80,0x03,0xC0 };

	module_t m_exeMod{};
	uintptr_t m_patchAtkAddr{};
	uintptr_t m_patchElementAddr{};
	bool m_enabled{};

	std::vector<uintptr_t> m_antiTemperAddrs{};
	std::vector<uintptr_t> m_patchFreeElementAddrs{};

	bool loadGame()
	{
		std::vector<PROCESSENTRY32W> entries{ process_t::getEntriesByName(L"MonsterHunterWorld.exe") };
		size_t size{ entries.size() };
		if (entries.size() == 0)
			writeln(LR"(No process named "MonsterHunterWorld.exe" found)");
		else if (entries.size() > 1)
			writeln(LR"(More than one processes named "MonsterHunterWorld.exe" found)");
		else if (entries.size() == 1)
		{
			const PROCESSENTRY32W& entry{ entries[0] };
			writeln(LR"(One process named "MonsterHunterWorld.exe" found)");
			uint32_t errorId{ 0 };
			if (m_game.loadByEntry(entries[0], &errorId))
			{
				writeln(LR"("MonsterHunterWorld.exe" is loaded, PID = )", m_game.pid());
				return true;
			}
			else
			{
				DWORD pid{ entry.th32ProcessID };
				writeln(LR"(Failed to load "MonsterHunterWorld.exe", PID = )", pid, L", Handle = ", m_game.handle(), L", ErrorID = ", errorId);
				return false;
			}

		}
		return false;
	}

	bool getExeModule(module_t& result)
	{
		std::vector<module_t> mods{ m_game.getModulesByName(L"MonsterHunterWorld.exe") };

		writeln(L"Found ", mods.size(), LR"( modules named "MonsterHunterWorld.exe" inside the process "MonsterHunterWorld.exe")");

		for (const module_t& mod : mods)
			writeln(mod);

		if (mods.size() == 0 || mods.size() > 1)
			return false;

		result = std::move(mods[0]);
		return true;
	}

	bool getPatchAtkAddr(uintptr_t& result)
	{
		//MonsterHunterWorld.Ordinal292+1530C90 - 0F57 C0               - xorps xmm0,xmm0
		//MonsterHunterWorld.Ordinal292+1530C93 - 44 8B C1              - mov r8d,ecx
		//MonsterHunterWorld.Ordinal292+1530C96 - 8B C2                 - mov eax,edx
		//MonsterHunterWorld.Ordinal292+1530C98 - 48 8D 0D E1148501     - lea rcx,[MonsterHunterWorld.AK::StreamMgr::SetFileLocationResolver+79EF80] { [4.80] }
		//MonsterHunterWorld.Ordinal292+1530C9F - F3 48 0F2A C0         - cvtsi2ss xmm0,rax
		//MonsterHunterWorld.Ordinal292+1530CA4 - F3 42 0F59 04 81      - mulss xmm0,[rcx+r8*4] <================== patch location
		//MonsterHunterWorld.Ordinal292+1530CAA - E9 D19B3A01           - jmp MonsterHunterWorld.AK::StreamMgr::SetFileLocationResolver+2F7680

		std::vector<uintptr_t> addrs{ m_game.findAddresses(cx_pattern("f 57 c0 44 8b c1 8b c2 48 8d d ? ? ? ? f3 48 0f 2a c0 f3 42 f 59 4 81 e9 ? ? ? ?"),m_exeMod) };

		if (addrs.size() == 0 || addrs.size() > 1)
			return false;

		result = addrs[0] + 20;
		writeln("Patch atk address found: ", std::hex, "0x", result);
		return true;
	}

	bool getPatchElementAddr(uintptr_t& result)
	{
		//MonsterHunterWorld.Ordinal292+1530CBC - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CBD - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CBE - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CBF - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CC0 - 8D 04 89              - lea eax,[rcx+rcx*4] <================== patch location
		//MonsterHunterWorld.Ordinal292+1530CC3 - 03 C0                 - add eax,eax
		//MonsterHunterWorld.Ordinal292+1530CC5 - C3                    - ret 
		//MonsterHunterWorld.Ordinal292+1530CC6 - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CC7 - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CC8 - CC                    - int 3 
		//MonsterHunterWorld.Ordinal292+1530CC9 - CC                    - int 3 

		std::vector<uintptr_t> addrs{ m_game.findAddresses(cx_pattern("cc cc cc cc 8d 04 89 03 c0 c3 cc cc cc cc cc"),m_exeMod) };

		if (addrs.size() == 0 || addrs.size() > 1)
			return false;

		result = addrs[0] + 4;
		writeln("Patch element address found: ", std::hex, "0x", result);
		return true;
	}

	bool getPatchFreeElementAddrs(std::vector<uintptr_t>& result)
	{
		//MonsterHunterWorld.Ordinal292+155CF20 - 48 83 EC 28           - sub rsp,28 { 40 }
		//MonsterHunterWorld.Ordinal292+155CF24 - 8B C1                 - mov eax,ecx
		//MonsterHunterWorld.Ordinal292+155CF26 - 83 FA 01              - cmp edx,01 { 1 }
		//MonsterHunterWorld.Ordinal292+155CF29 - 75 1F                 - jne MonsterHunterWorld.Ordinal292+155CF4A
		//MonsterHunterWorld.Ordinal292+155CF2B - 0F57 C0               - xorps xmm0,xmm0
		//MonsterHunterWorld.Ordinal292+155CF2E - F3 48 0F2A C0         - cvtsi2ss xmm0,rax
		//MonsterHunterWorld.Ordinal292+155CF33 - F3 0F59 05 75976E01   - mulss xmm0,[MonsterHunterWorld.AK::StreamMgr::SetFileLocationResolver+620620] { [0.33] }
		//MonsterHunterWorld.Ordinal292+155CF3B - E8 B0767300           - call MonsterHunterWorld.Ordinal292+1C945F0
		//MonsterHunterWorld.Ordinal292+155CF40 - 8D 04 80              - lea eax,[rax+rax*4] <================== patch location
		//MonsterHunterWorld.Ordinal292+155CF43 - 03 C0                 - add eax,eax
		//MonsterHunterWorld.Ordinal292+155CF45 - 48 83 C4 28           - add rsp,28 { 40 }
		//MonsterHunterWorld.Ordinal292+155CF49 - C3                    - ret 
		//MonsterHunterWorld.Ordinal292+155CF4A - 83 FA 02              - cmp edx,02 { 2 }
		//MonsterHunterWorld.Ordinal292+155CF4D - 75 23                 - jne MonsterHunterWorld.Ordinal292+155CF72
		//MonsterHunterWorld.Ordinal292+155CF4F - 0F57 C0               - xorps xmm0,xmm0
		//MonsterHunterWorld.Ordinal292+155CF52 - F3 48 0F2A C0         - cvtsi2ss xmm0,rax
		//MonsterHunterWorld.Ordinal292+155CF57 - F3 0F59 05 51976E01   - mulss xmm0,[MonsterHunterWorld.AK::StreamMgr::SetFileLocationResolver+620620] { [0.33] }
		//MonsterHunterWorld.Ordinal292+155CF5F - F3 0F58 C0            - addss xmm0,xmm0
		//MonsterHunterWorld.Ordinal292+155CF63 - E8 88767300           - call MonsterHunterWorld.Ordinal292+1C945F0
		//MonsterHunterWorld.Ordinal292+155CF68 - 8D 04 80              - lea eax,[rax+rax*4] <================== patch location
		//MonsterHunterWorld.Ordinal292+155CF6B - 03 C0                 - add eax,eax
		//MonsterHunterWorld.Ordinal292+155CF6D - 48 83 C4 28           - add rsp,28 { 40 }
		//MonsterHunterWorld.Ordinal292+155CF71 - C3                    - ret 
		//MonsterHunterWorld.Ordinal292+155CF72 - 83 FA 03              - cmp edx,03 { 3 }
		//MonsterHunterWorld.Ordinal292+155CF75 - 0F44 C0               - cmove eax,eax
		//MonsterHunterWorld.Ordinal292+155CF78 - 8D 04 80              - lea eax,[rax+rax*4] <================== patch location
		//MonsterHunterWorld.Ordinal292+155CF7B - 03 C0                 - add eax,eax
		//MonsterHunterWorld.Ordinal292+155CF7D - 48 83 C4 28           - add rsp,28 { 40 }
		//MonsterHunterWorld.Ordinal292+155CF81 - C3                    - ret 

		result = m_game.findAddresses(cx_pattern("8D 04 80 03 C0 48 83 C4 ? C3"), m_exeMod);

		if (result.size() != 3)
			return false;

		std::wostringstream ss;
		ss << "Patch free element address found: [" << std::hex;
		for (size_t i{}; i < result.size() - 1; ++i)ss << "0x" << result[i] << ',';
		ss << "0x" << result.back()<<']';

		writeln(ss.str());
		return true;
	}

	bool getAntiTemperAddrs(std::vector<uintptr_t>& result)
	{
		std::vector<uintptr_t> possibleAddrs{ m_game.findAddresses(cx_pattern("8B 02 48 8D 52 04 48 33 D8"), m_exeMod) };
		writeln("Anti temper pattern scan result count:", possibleAddrs.size());

		std::array<uint8_t, 203> buffer;
		for (uintptr_t addr : possibleAddrs)
		{
			if (!m_game.read(addr, buffer))return false;

			for (size_t i{}; i != 200; ++i)
				if (*reinterpret_cast<const std::array<uint8_t, 3>*>(&buffer[i]) == m_originalBytesAntiTemper)
				{
					result.push_back(addr + i);
					*reinterpret_cast<std::array<uint8_t, 3>*>(&buffer[i]) = m_patchBytesAntiTemper;
				}
		}
		writeln("Anti temper addresses count = ", result.size());

		return true;
	}

	bool patchAtk() { return m_game.write(m_patchAtkAddr, m_patchBytesAtk); }

	bool patchElement() { return m_game.write(m_patchElementAddr, m_patchBytesElement); }

	bool patchFreeElement()
	{
		for (uintptr_t addr : m_patchFreeElementAddrs)
			if(!m_game.write(addr, m_patchBytesFreeElement))
				return false;
		return true;
	}

	bool patchAntiTemper()
	{
		size_t count{};
		for (uintptr_t addr : m_antiTemperAddrs)
			count += m_game.write(addr, m_patchBytesAntiTemper) ? 1 : 0;
		writeln("Total anti-temper patch count = ", count);
		return true;
	}

	bool unpatchAtk() { return m_game.write(m_patchAtkAddr, m_originalBytesAtk); }
	bool unpatchElement() { return m_game.write(m_patchElementAddr, m_originalBytesElement); }
	bool unpatchAntiTemper(const std::vector<uintptr_t>& addrs)
	{
		size_t count{};
		for (uintptr_t addr : addrs)
			count += m_game.write(addr, m_originalBytesAntiTemper) ? 1 : 0;
		writeln("Total anti-temper patch count = ", count);
		return true;
	}
	bool unpatchFreeElement()
	{
		for (uintptr_t addr : m_patchFreeElementAddrs)
			if (!m_game.write(addr, m_originalBytesFreeElement))
				return false;
		return true;
	}

	bool enable()
	{
		if (!loadGame()) { writeln("loadGame failed"); return false; }
		if (!getExeModule(m_exeMod)) { writeln("getExeModule failed"); return false; }
		if (!getAntiTemperAddrs(m_antiTemperAddrs)) { writeln("getAntiTemperAddrs failed"); return false; }

		if (!getPatchAtkAddr(m_patchAtkAddr)) { writeln("getPatchLocation failed"); return false; }
		if (!getPatchElementAddr(m_patchElementAddr)) { writeln("getPatchElementAddr failed"); return false; }
		if (!getPatchFreeElementAddrs(m_patchFreeElementAddrs)) { writeln("getPatchFreeElementAddrs failed"); return false; }

		if (!patchAntiTemper()) { writeln("patchAntiTemper failed"); return false; }
		if (!patchAtk()) { writeln("patchAtk failed"); return false; }
		if (!patchElement()) { writeln("patchElement failed"); return false; }
		if (!patchFreeElement()) { writeln("patchFreeElement failed"); return false; }

		return true;
	}
};
