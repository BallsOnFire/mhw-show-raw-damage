#pragma once
#include <winsock2.h>
#include <Psapi.h> //GetModuleBaseNameW 
#include <TlHelp32.h>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <type_traits>
#include <iomanip>
#include <functional>
#include <string>
#include <sstream>

#include "pattern.h"

namespace Windows
{
	struct Process
	{
	public:

		struct Module
		{
		private:
			uintptr_t m_base;
			size_t m_size;
			std::wstring m_name;
		public:
			Module(const MODULEENTRY32W& entry) :
				m_base{ reinterpret_cast<uintptr_t>(entry.modBaseAddr) },
				m_size{ entry.modBaseSize },
				m_name{ entry.szModule }
			{}

			Module(uintptr_t base, size_t size, std::wstring name) :
				m_base{ base },
				m_size{ size },
				m_name{ name }
			{}

			Module() = default;
			Module(Module&&) = default;
			Module& operator=(Module&&) = default;

			const std::wstring& name()const { return m_name; }
			uintptr_t base()const { return m_base; }
			size_t size()const { return m_size; }

			bool operator<(const Module& rhs)const { return m_base < rhs.m_base; }
			bool operator>(const Module& rhs)const { return m_base > rhs.m_base; }

			friend auto& operator<<(std::wostream& os, const Module& mod)
			{
				os << std::hex;
				os << L"Base = " << std::setfill(L'0') << std::setw(sizeof(uintptr_t) * 2) << mod.m_base;
				os << L", End = " << std::setfill(L'0') << std::setw(sizeof(uintptr_t) * 2) << mod.m_base + mod.m_size;
				os << L", Name = " << mod.m_name;
				return os;
			}
		private:

			Module(const Module&) = default;
			Module & operator=(const Module&) = default;
		};

		struct Region
		{
			uintptr_t rpBegin;
			uintptr_t allocBase;
			DWORD allocProtect;
			size_t size;
			DWORD state;
			DWORD protect;
			DWORD type;
			uintptr_t rpEnd;

			Region(MEMORY_BASIC_INFORMATION memInfo) :
				rpBegin{ reinterpret_cast<uintptr_t>(memInfo.BaseAddress) },
				allocBase{ reinterpret_cast<uintptr_t>(memInfo.AllocationBase) },
				allocProtect{ memInfo.AllocationProtect },
				size{ memInfo.RegionSize },
				state{ memInfo.State },
				protect{ memInfo.Protect },
				type{ memInfo.Type },
				rpEnd{ rpBegin + size }
			{}

			friend auto& operator<<(std::wostream& os, const Region& region)
			{
				os << std::hex;
				os << L"Base = " << std::setfill(L'0') << std::setw(sizeof(uintptr_t) * 2) << region.rpBegin;
				os << L", End = " << std::setfill(L'0') << std::setw(sizeof(uintptr_t) * 2) << region.rpEnd;
				return os;
			}
		};

		struct ScanRegion
		{
			uintptr_t rpBegin;
			uintptr_t rpEnd;
			ScanRegion(const Region& region) :rpBegin{ region.rpBegin }, rpEnd{ region.rpEnd }{}

			bool append(const Region& region)
			{
				if (region.rpBegin == rpEnd)
				{
					rpEnd += region.size;
					return true;
				}
				return false;
			}
			size_t size()const { return rpEnd - rpBegin; }

			friend auto& operator<<(std::wostream& os, const ScanRegion& region)
			{
				os << std::hex;
				os << L"Base = " << std::setfill(L'0') << std::setw(sizeof(uintptr_t) * 2) << region.rpBegin;
				os << L", End = " << std::setfill(L'0') << std::setw(sizeof(uintptr_t) * 2) << region.rpEnd;
				return os;
			}
		};

	private:
		DWORD m_pid;
		HANDLE m_handle;
		bool m_is64;
	public:
		Process() :m_pid{ 0 }, m_handle{ INVALID_HANDLE_VALUE }, m_is64{ false } {}
		~Process() { dispose(); }
		Process(Process&&) = delete;
		Process(const Process&) = delete;
		Process& operator=(Process&&) = delete;
		Process& operator=(const Process&) = delete;

		const HANDLE& handle()const { return m_handle; }
		DWORD pid()const { return m_pid; }
		bool is64()const { return m_is64; }

		bool loadByEntry(const PROCESSENTRY32W& entry, uint32_t* pErrorID = nullptr)
		{
			dispose();
			m_pid = entry.th32ProcessID;
			m_handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
			if (m_handle == 0)
			{
				if (pErrorID)*pErrorID = GetLastError();
				m_handle = INVALID_HANDLE_VALUE;
				return false;
			}

			BOOL is64;
			if (!IsWow64Process(m_handle, &is64))
			{
				if (pErrorID)*pErrorID = GetLastError();
				dispose();
				return false;
			}
			m_is64 = is64 == 0;

			return true;
		}

		std::vector<Region> getRegions(uintptr_t rpBegin, uintptr_t rpEnd)const
		{
			std::vector<Region> result;
			uintptr_t addr{ rpBegin };
			MEMORY_BASIC_INFORMATION memoryInfo{};
			for (;;)
			{
				if (0 == VirtualQueryEx(m_handle, reinterpret_cast<const void*>(addr), &memoryInfo, sizeof(memoryInfo))) break;
				if ((memoryInfo.State & MEM_COMMIT) && memoryInfo.RegionSize != 0)
					result.emplace_back(memoryInfo);
				addr += memoryInfo.RegionSize;
				if (addr > rpEnd)
					break;
			}
			return result;
		}

		std::vector<ScanRegion> getScanRegions(const std::vector<Region>& regions)const
		{
			size_t size{ regions.size() };
			if (size == 0)return {};

			std::vector<ScanRegion> result{ regions[0] };
			for (size_t i{ 1 }; i != size; ++i)
				if (!result.back().append(regions[i]))
					result.emplace_back(regions[i]);
			return result;
		}

		std::vector<ScanRegion> getScanRegions(uintptr_t rpBegin, uintptr_t rpEnd)const
		{
			return getScanRegions(getRegions(rpBegin, rpEnd));
		}

		template<class Lambda>
		std::vector<Process::Module> getModulesEx(Lambda&& isMatch)const
		{
			MODULEENTRY32W moduleEntry{}; moduleEntry.dwSize = sizeof(moduleEntry);
			HANDLE hSnapshot{ CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, m_pid) };
			if (hSnapshot == INVALID_HANDLE_VALUE)
				return {};
			std::vector<Process::Module> result;
			if (Module32FirstW(hSnapshot, &moduleEntry))
			{
				do
				{
					Process::Module newMod{ moduleEntry };
					if (isMatch(newMod))
						result.push_back(moduleEntry);
				} while (Module32NextW(hSnapshot, &moduleEntry));
			}
			CloseHandle(hSnapshot);
			std::sort(result.begin(), result.end());
			return result;
		}

		std::vector<Process::Module> getModulesByName(const wchar_t* name)const
		{
			return getModulesEx([&](const Process::Module& mod) { return mod.name() == name; });
		}

		template<class T> bool write(uintptr_t address, const T& input)const
		{
			static_assert(!std::is_pointer_v<T>);
			return WriteProcessMemory(m_handle, reinterpret_cast<void*>(address), &input, sizeof(input), nullptr);
		}

		template<class T> bool read(uintptr_t address, T& output)const
		{
			static_assert(!std::is_pointer_v<T>);
			return ReadProcessMemory(m_handle, reinterpret_cast<void*>(address), &output, sizeof(T), nullptr);
		}

		template<class T> bool read(uintptr_t address, T* pOutput, size_t sizeInBytes)const
		{
			return ReadProcessMemory(m_handle, reinterpret_cast<void*>(address), (void*)(pOutput), sizeInBytes, nullptr);
		}

		template<class T> bool readStdVector(uintptr_t address, std::vector<T>& output)const { return read(address, &output[0], output.size() * sizeof(T)); }

	private:
		template<class cx_pattern_t>
		void findAddressesEx(cx_pattern_t pattern, uintptr_t rpBegin, size_t size, std::vector<uintptr_t>& result)const
		{
			if (size)
			{
				std::vector<uint8_t> buffer(size);
				if (!readStdVector(rpBegin, buffer))return;

				std::vector<const uint8_t*> ptrs{ pattern.findAll(&buffer[0], size) };

				for (uintptr_t& addr : reinterpret_cast<std::vector<uintptr_t>&>(ptrs))
					result.push_back(addr - reinterpret_cast<uintptr_t>(&buffer[0]) + rpBegin);
			}
		}
	public:

		template<class cx_pattern_t>
		std::vector<uintptr_t> findAddresses(cx_pattern_t pattern, const std::vector<ScanRegion>& regions)const
		{
			std::vector<uintptr_t> result;

			for (const ScanRegion& region : regions)
				findAddressesEx(pattern, region.rpBegin, region.size(), result);

			return result;
		}

		template<class cx_pattern_t>
		std::vector<uintptr_t> findAddresses(cx_pattern_t pattern, uintptr_t rpBegin, uintptr_t rpEnd)const
		{
			std::vector<ScanRegion> regions{ getScanRegions(rpBegin,rpEnd) };
			if (regions.size())
			{
				ScanRegion& lastReg{ regions.back() };
				if (lastReg.rpEnd > rpEnd)
					lastReg.rpEnd = rpEnd;
				return findAddresses(pattern, regions);
			}
			else
				return {};
		}

		template<class cx_pattern_t>
		std::vector<uintptr_t> findAddresses(cx_pattern_t pattern, const Process::Module& mod)const
		{
			return findAddresses(pattern, mod.base(), mod.base() + mod.size());
		}

		static std::vector<PROCESSENTRY32W> getEntriesByName(const wchar_t*targetProcessName)
		{
			return getEntries([&](const PROCESSENTRY32W& pe) { return wcscmp(targetProcessName, pe.szExeFile) == 0; });
		}

		template<class Lambda>
		static std::vector<PROCESSENTRY32W> getEntries(Lambda&& fIsMatch)
		{
			std::vector<PROCESSENTRY32W> result;
			HANDLE handleSnap{ CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) };
			if (handleSnap == INVALID_HANDLE_VALUE)
				return result;
			PROCESSENTRY32W pe32; pe32.dwSize = sizeof(pe32);
			if (!Process32FirstW(handleSnap, &pe32))
			{
				CloseHandle(handleSnap);
				return result;
			}

			do
			{
				if (fIsMatch(pe32))
					result.push_back(pe32);
			} while (Process32NextW(handleSnap, &pe32));
			CloseHandle(handleSnap);
			return result;
		}

	private:
		void dispose()
		{
			if (m_handle != INVALID_HANDLE_VALUE)
			{
				CloseHandle(m_handle);
				m_handle = INVALID_HANDLE_VALUE;
			}
		}
	};
}

