#pragma once
#include <stdint.h>
#include <utility>
#include <vector>

template<class... Args>constexpr decltype(auto) all(Args&&... args) { return (... &&std::forward<Args>(args)); }

template<std::size_t N, class T, class... types>struct nth_type { using type = typename nth_type<N - 1, types...>::type; };
template<class T, class... types>struct nth_type<0, T, types...> { using type = T; };
template<std::size_t N, class...Args>using nth_type_t = typename nth_type<N, Args...>::type;

template<class T, std::size_t N, T First, T...Rest>struct nth_value { static constexpr T value = nth_value<T, N - 1, Rest...>::value; };
template<class T, T First, T...Rest>struct nth_value<T, 0, First, Rest...> { static constexpr T value = First; };
template<class T, std::size_t N, T...Rest>constexpr T nth_value_v = nth_value<T, N, Rest...>::value;

template<class T, T...Cs>
struct CxString
{
private:
	using base_t = CxString<T, Cs...>;
public:
	static constexpr std::size_t Size{ sizeof...(Cs) };

	template<std::size_t Index>
	static constexpr T at()
	{
		static_assert(Index < Size);
		return nth_value_v<T, Index, Cs...>;
	}
};

template<class char_type, class lambda_t, size_t...I>
constexpr decltype(auto) make_cx_string(lambda_t lambda, std::index_sequence<I...>)
{
	return CxString < char_type, lambda()[I]... > {};
}

#define XS(str)	(make_cx_string<char>([]()constexpr{return(str);},std::make_index_sequence<sizeof(str)/sizeof(char) - 1>{}))
#define WXS(str) (make_cx_string<wchar_t>([]()constexpr{return(str);},std::make_index_sequence<sizeof(str)/sizeof(wchar_t) - 1>{}))

template<bool Check, uint8_t Value>
struct CxPatternElement
{
	static constexpr bool isMatch(const uint8_t* ptr)noexcept
	{
		if constexpr (Check)
			return *ptr == Value;
		else
			return true;
	}
};

template<class...Elements>
struct CxPattern
{
protected:
	using base_t = CxPattern<Elements...>;
public:

	template<class Element>
	static constexpr CxPattern<Elements..., Element> append()noexcept { return {}; }

	template<std::size_t Index>
	static constexpr decltype(auto) at()noexcept { return nth_type_t<Index, Elements...>{}; }

	static constexpr std::size_t Size{ sizeof...(Elements) };

	template<std::size_t ...I>
	static constexpr bool isMatchEx(const uint8_t* ptr, std::index_sequence<I...>)noexcept
	{
		return all((at<I>().isMatch(ptr + I))...);
	}

	static constexpr bool isMatch(const uint8_t* ptr)noexcept
	{
		return isMatchEx(ptr, std::make_index_sequence<Size>());
	}

	static std::vector<const uint8_t*> findAll(const uint8_t* pBegin, std::size_t size)noexcept
	{
		std::vector<const uint8_t*> result;
		const uint8_t* pEnd{ pBegin + size - base_t::Size + 1 };

		for (const uint8_t* ptr{ pBegin }; ptr < pEnd; ++ptr)
			if (isMatch(ptr))
				result.push_back(ptr);

		return result;
	}

	static constexpr const uint8_t* findFirst(const uint8_t* pBegin, std::size_t size)noexcept
	{
		const uint8_t* pEnd{ pBegin + size - base_t::Size + 1 };

		for (const uint8_t* ptr{ pBegin }; ptr < pEnd; ++ptr)
			if (isMatch(ptr))
				return ptr;

		return nullptr;
	}
};

template<char C>
constexpr bool cxIshexDigit()noexcept
{
	constexpr bool is_number = C >= '0'&&C <= '9';
	constexpr bool is_abcdef = C >= 'a'&&C <= 'f';
	constexpr bool is_ABCDEF = C >= 'A'&&C <= 'F';
	return is_number || is_abcdef || is_ABCDEF;
}

template<char C>
constexpr uint8_t cxChar2hexDigit()noexcept
{
	static_assert(cxIshexDigit<C>());
	if constexpr (C >= '0'&&C <= '9') return C - '0';
	if constexpr (C >= 'a'&&C <= 'f') return C - 'a' + 10;
	if constexpr (C >= 'A'&&C <= 'F') return C - 'A' + 10;
}

template<class CxStr, std::size_t Index, class Result>
constexpr decltype(auto) makeCxPatternEx()noexcept
{
	if constexpr (CxStr::Size == Index)return Result{};
	else
	{
		constexpr char c = CxStr::template at<Index>();
		if constexpr (c == '\t' || c == ' ')
			return makeCxPatternEx<CxStr, Index + 1, Result>();
		else
		{
			if constexpr (c == '?')
			{
				constexpr std::size_t IndexNext{ Index + 1 };
				if constexpr (CxStr::Size != IndexNext && CxStr::template at<IndexNext>() == '?')
					return makeCxPatternEx<CxStr, Index + 2, decltype(Result::template append<CxPatternElement<false, 0>>())>();
				else
					return makeCxPatternEx<CxStr, Index + 1, decltype(Result::template append<CxPatternElement<false, 0>>())>();
			}
			else
			{
				constexpr uint8_t digit0{ cxChar2hexDigit<c>() };
				constexpr std::size_t IndexNext{ Index + 1 };

				if constexpr (CxStr::Size == IndexNext)
					return makeCxPatternEx<CxStr, Index + 1, decltype(Result::template append<CxPatternElement<true, digit0>>())>();
				else if constexpr (!cxIshexDigit<CxStr::template at<IndexNext>()>())
					return makeCxPatternEx<CxStr, Index + 1, decltype(Result::template append<CxPatternElement<true, digit0>>())>();
				else
				{
					constexpr char nextChar{ CxStr::template at<IndexNext>() };
					constexpr uint8_t digit1{ cxChar2hexDigit<nextChar>() };
					constexpr uint8_t value{ digit0 * 16 + digit1 };
					return makeCxPatternEx<CxStr, Index + 2, decltype(Result::template append<CxPatternElement<true, value>>())>();
				}
			}
		}
	}
}

template<class CxStr>
constexpr decltype(auto) makeCxPattern(CxStr)noexcept
{
	return makeCxPatternEx<CxStr, 0, CxPattern<>>();
}

#define cx_pattern(str) makeCxPattern(XS(str))